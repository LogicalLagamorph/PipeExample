// Author :Wesley Leach
// Date : 9-19-15

/*
* This is an example of how a pipe can be used to communicate between
 two processes. It is an example because obviously they are both running inside
 the same process. Later in this class I assume we will get to multi-threading
  and use this code as a base to run a proper pipe example.
*/
#include <iostream>
#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
using namespace std;

// Global definition of Pipe
#define ARRAY_SIZE 100
char Pipe[ARRAY_SIZE];
// Positions to read and write from for the circular array.
int readPosition=0;
int writePosition=0;

void writeToPipe(int inputChar){
    Pipe[writePosition]=inputChar;
    // Keeps the writePosition within bounds of the array.
    writePosition= ( writePosition+1 ) % ARRAY_SIZE;

}

char readFromPipe(){
    char charReturn = Pipe[readPosition];
    // clears the position of the pipe
    Pipe[readPosition] = NULL;
    readPosition = ( readPosition +1 ) % ARRAY_SIZE;
    return charReturn;

}
class Producer{
public:
    void newStocks(){
        for(int i=0; i<10;i++){
        int prices = rand() % 10000;
        writeToPipe(prices);
        }
    }
};

class Consumer{
private:
    int balance;
public:
    Consumer(){
        // Starting cash.
        balance = 100000;
        run();
    }
    void run(){
        int x=0;
        Producer stocks;
        while(x<1000){
            stocks.newStocks();
            buy();
            sell();
            cout<<"Day: "<<x<<". Balance = "<<balance<<endl;
            x++;
        }
    }
    void buy(){
        //Buys if price is even because I don't want to make an
        // Ai for no points.
        int Price = readFromPipe();
        if( Price % 2 == 0){
            balance = balance - Price *2;
        }
        else{
            // If price is odd it will put the item back.
            writeToPipe( Price );
        }
    }
    void sell(){
        // If price is odd he will see the given stock because why not.
        int Price = readFromPipe();
        if( Price % 2 != 0){
            balance = balance + Price *2;
            writeToPipe(Price);
        }
    }

};

int main()
{
    pid_t pID = fork();
    Consumer* broker= new Consumer();
    return 0;
}
